"use strict";


/*1. Опишіть своїми словами як працює метод forEach.

	Метод forEach - это один из методов перебора массива. Двигаясь от начала к концу массива, метод находит каждый элемент, передает его в коллбек функцию и 
	запускает ее на каждом шаге для каждого элемента массива. При этом сам метод, после перебора массива ничего не возвращает. 
*/

/* 2. Як очистити масив??
	Если массив был объявлен через let, то можно очистить его с помощью переприсвоения переменной пустого массива (arr = []). 
	Если массив был объявлен как константа const, то его можно очистить задав нулевую длину (arr.length = 0). Но этот способ очистит только итерируемые свойства с целочисленными индексами.
	Например, const arr = [1, 2, 3, a:4, b:5] ---> arr.length = 0 ---> console.log(arr) ---> [a:4, b:5]. 
*/

/* 3. Як можна перевірити, що та чи інша змінна є масивом?

	Нужно обратится к конструктору Array и вызвать метод isArray, который принимает в качестве аргумента проверяемый объект и возвращает true или false. (Array.isArray([1,2,3]) ---> true) 
*/


const someArr = [null, 5, -3, 8n, 'string', '0', 'null', ' ', Math, Date, 'undefined', '', true, false, undefined, [1, '2', true], { a: 15, b: '3' }];

const typeFilter = (arr, dataType) => {
	const filteredArr = arr.filter(el => el !== null ? typeof (el) !== dataType : dataType === 'null' ? null : `${el}`);
	return filteredArr;
}

const TEST_ALL_TYPES = (arr) => {
	const allTypes = ['string', 'number', 'bigint', 'boolean', 'null', 'undefined', 'object', 'function'];

	return allTypes.map(type => {
		console.log(`Исключены все '${type}'`, typeFilter(arr, type));
		return typeFilter(arr, type)
	})
}

console.log('Исходный массив', someArr);
TEST_ALL_TYPES(someArr)

